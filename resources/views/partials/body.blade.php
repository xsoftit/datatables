<div class="table-container">
    <table id='{{$dataTable->name()}}' class="{{$dataTable->cssClass()}}">
        <thead>
        <tr>
            @foreach($dataTable->columns() as $column)
                <th>{!! $column->get('name') !!}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
