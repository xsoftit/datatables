<?php

namespace Xsoft\DataTables;

use Illuminate\Support\Facades\Schema;
use Xsoft\DataTables\Services\DataTableService;
use Exception;

class DataTable
{
    protected $modelOrTable;

    protected $isModel = true;

    protected $route;

    protected $columns;

    protected $options;

    protected $query;

    protected $routeParams = [];

    protected $ajaxParams = [];

    protected $orderBy = [];

    protected $searchBy = [];

    protected $join = [];

    public $noSection = false;

    public $generate = true;

    public function __construct($modelOrTable, $route, $columns = [], $options = [], $params = [])
    {
        $this->setModelOrTable($modelOrTable);
        $this->setRoute($route);
        $this->setColumns($columns);
        $this->setOptions($options);
        $this->addParams($params);
    }

    // Getters
    public function modelOrTable()
    {
        return $this->modelOrTable;
    }

    public function route()
    {
        return $this->route;
    }

    public function columns()
    {
        return $this->columns;
    }

    public function options()
    {
        return $this->options;
    }

    public function params($type = 'ajax')
    {
        if ($type == 'ajax') {
            return $this->ajaxParams;
        } else if ($type == 'route') {
            return $this->routeParams;
        } else {
            return [];
        }
    }

    public function getOrderBy(){
        return $this->orderBy;
    }

    public function getSearchBy(){
        return $this->searchBy;
    }

    public function getJoin(){
        return $this->join;
    }

    public function query($request = null)
    {
        if (!$this->query && $request) {
            $this->query = DataTableService::createQuery($request, $this);
        }
        return $this->query;
    }

    public function isModel(){
        return $this->isModel;
    }

    // Setters
    public function setRoute($route)
    {
        $this->route = $route;
        return $this;
    }

    public function orderBy($orderBy){
        $this->orderBy = $orderBy;
        return $this;
    }

    public function searchBy(...$searchBy){
        $this->searchBy = $searchBy;
        return $this;
    }

    public function join(...$join){
        $this->join = $join;
        return $this;
    }

    public function setModelOrTable($modelOrTable)
    {
        if (!class_exists($modelOrTable)) {
            $this->isModel = false;
            if(!Schema::hasTable($modelOrTable)){
                throw new Exception('Table or Model "'.$modelOrTable.'" not found');
            }
        }
        $this->modelOrTable = $modelOrTable;
        return $this;
    }

    public function setColumns($columns)
    {
        $this->columns = DataTableService::createColumns($this, $columns);
        return $this;
    }

    public function addColumn($column){
        $this->columns = $this->columns->push(DataTableService::createColumn($column));
        return $this;
    }

    public function setOptions($options)
    {
        $this->options = $options;
        return $this;
    }

    public function setQuery($query)
    {
        $this->query = $query;
        return $this;
    }

    public function setTable($table)
    {
        $this->service->setTable($table);
        return $this;
    }

    public function addParams($params)
    {
        foreach ($params as $key => $param) {
            if (is_array($param)) {
                if (key_exists('1', $param)) {
                    if ($param['1'] == 'route') {
                        $this->routeParams[$key] = $param['0'];
                    } else {
                        $this->ajaxParams[$key] = $param['0'];
                    }
                } else {
                    $this->ajaxParams[$key] = $param['0'];
                }
            } else {
                $this->ajaxParams[$key] = $param;
            }
        }

        return $this;
    }

    //pobiera wartość z atrybutu 'options'
    public function get($attribute, $isArray = false)
    {
        if (key_exists($attribute, $this->options)) {
            return $this->options[$attribute];
        }
        if ($isArray) {
            return [];
        }
        return false;
    }

    public function name()
    {
        return DataTableService::getTableName($this);
    }

    public function generateName()
    {
        return 'generate' . ucfirst(DataTableService::getTableName($this));
    }

    public function draw()
    {
        if(view()->exists('datatables.datatable')){
            return view('datatables.datatable', ['dataTable' => $this]);
        }else{
            return view('datatables::datatable', ['dataTable' => $this]);
        }
    }

    public function cssClass()
    {
        if (key_exists('classes', $this->options)) {
            return $this->options['classes'];
        } else {
            return 'table';
        }
    }

    public function output($request)
    {
        return DataTableService::createOutput($request, $this);
    }

}
