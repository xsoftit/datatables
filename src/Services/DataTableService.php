<?php

namespace Xsoft\DataTables\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class DataTableService
{
    const COLUMN_REMOVE = ['created_at', 'updated_at', 'deleted_at'];

    public static function createColumns(&$dataTable, $columns)
    {
        $newColumns = collect([]);
        if (empty($columns)) {
            $newColumns = self::createColumnsFrom($dataTable);
        } else {
            foreach ($columns as $column) {
                $newColumns->push(self::createColumn($column));
            }
        }
        return $newColumns;
    }

    public static function createColumn($column)
    {
        if (is_array($column)) {
            $newColumn = new DataTableColumn();
            $newColumn->set($column);
            return $newColumn;
        } else if ($column instanceof DataTableColumn) {
            return $column;
        }
        return null;
    }

    public static function createQuery($request, $dataTable)
    {
        $pageNum = $request['start'] / $request['length'] + 1;
        $query = $dataTable->query();
        $columns = $dataTable->columns();
        $orderBy = $dataTable->getOrderBy();
        self::handleOrderBy($orderBy, $columns, $request);
        if (!$query) {
            if ($dataTable->isModel()) {
                $query = $dataTable->modelOrTable()::orderBy($orderBy[0], $orderBy[1]);
            } else {
                $query = DB::table($dataTable->modelOrTable())->orderBy($orderBy[0], $orderBy[1]);
            }
        } else {
            $query = $query->orderBy($orderBy[0], $orderBy[1]);
        }
        self::handleJoin($query, $dataTable);
        self::handleSearch($query, $dataTable->getSearchBy(), $request);
        $query = $query->paginate($request['length'], ['*'], 'page', $pageNum);
        $dataTable->setQuery($query);
    }

    public static function createOutput($request, $dataTable)
    {
        self::createQuery($request, $dataTable);
        $output = [
            "draw" => $request->get('draw'),
            "recordsTotal" => $dataTable->query()->total(),
            "recordsFiltered" => $dataTable->query()->total(),
            "aaData" => [],
            "collection" => $dataTable->query()
        ];
        return $output;
    }

    public static function getTableName($dataTable)
    {
        $options = $dataTable->options();
        if (key_exists('name', $options)) {
            return $options['name'];
        } else {
            return strtolower(substr($dataTable->modelOrTable(), strrpos($dataTable->modelOrTable(), '\\') + 1)) . 'Table';
        }
    }

    private static function handleOrderBy(&$orderBy, $columns, $request)
    {
        if (!key_exists(0, $orderBy)) {
            $orderBy[0] = $columns->get($request['order'][0]['column'])->get('field', 0);
        } else {
            if (!$columns->where('name', $orderBy[0])) {
                if (!$columns->get($orderBy[0])) {
                    $orderBy[0] = $columns->get($request['order'][0]['column'])->get('field', 0);
                } else {
                    $orderBy[0] = $columns->get($orderBy[0])->get('field', 0);
                }
            }
        }
        if (!key_exists(1, $orderBy)) {
            $orderBy[1] = $request['order'][0]['dir'];
        } else {
            if ($orderBy[1] != 'asc' && $orderBy[1] != 'desc') {
                $orderBy[1] = $request['order'][0]['dir'];
            }
        }
    }

    private static function handleSearch(&$query, $searchBy, $request)
    {
        $searchValue = $request->get('search')['value'];
        $query = $query->where(function ($que) use ($searchValue, $searchBy) {
            foreach ($searchBy as $key => $column) {
                if ($key == 0) {
                    $que->where($column, 'LIKE', '%' . $searchValue . '%');
                } else {
                    $que->orWhere($column, 'LIKE', '%' . $searchValue . '%');
                }
            }
        });

    }

    private static function handleJoin(&$query, $dataTable)
    {
        if ($dataTable->isModel()) {
            $model = $dataTable->modelOrTable();
            $table = (new $model)->getTable();
        } else {
            $table = $dataTable->modelOrTable();
        }
        $joins = $dataTable->getJoin();
        if (!empty($joins)) {
            $query = $query->select($table . '.*');
        }
        foreach ($joins as $join) {
            if (is_array($join)) {
                if (key_exists(0, $join)) {
                    $joinTable = $join[0];
                } else {
                    continue;
                }
                if (key_exists(1, $join)) {
                    $foreignKey = $join[1];
                } else {
                    $foreignKey = $table.'.'.Str::singular($join) . '_id';
                }
                if (key_exists(2, $join)) {
                    $primarykey = $join[2];
                } else {
                    $primarykey = $joinTable . '.id';
                }
            } else {
                $joinTable = $join;
                $foreignKey = $table.'.'.Str::singular($join) . '_id';
                $primarykey = $joinTable . '.id';
            }
            $query = $query->leftjoin($joinTable, $primarykey, '=', $foreignKey);
        }
    }

    private static function createColumnsFrom($dataTable)
    {
        $newColumns = collect([]);
        $modelOrTable = $dataTable->modelOrTable();
        if ($dataTable->isModel()) {
            $table = with(new $modelOrTable)->getTable();
        } else {
            $table = $modelOrTable;
        }
        $columns = Schema::getColumnListing($table);
        foreach (self::COLUMN_REMOVE as $cr) {
            if (($key = array_search($cr, $columns)) !== false) {
                unset($columns[$key]);
            }
        }
        foreach ($columns as $column) {
            $newColumns->push(self::createColumn([ucfirst($column), $column]));
        }
        return $newColumns;
    }
}
