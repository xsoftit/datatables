<?php

namespace Xsoft\DataTables\Services;

class DataTableColumn
{
    protected $name = '';
    protected $field = '';
    protected $options = [];

    public function get($attribute, $query = 0){
        if($attribute == 'field'){
            if(is_array($this->field)){
                return $this->field[$query];
            }
        }
        return $this->$attribute;
    }

    public function setName($name){
        $this->name = $name;
        return $this;
    }

    public function setField($field){
        $this->field = $field;
        return $this;
    }

    public function setOptions($options){
        $this->options = $options;
        return $this;
    }

    public function set($data){
        if(key_exists('name',$data)){
            $this->name = $data['name'];
        }
        if(key_exists('0',$data)){
            $this->name = $data['0'];
        }
        if(key_exists('field',$data)){
            $this->field = $data['field'];
        }
        if(key_exists('1',$data)){
            $this->field = $data['1'];
        }
        if(key_exists('options',$data)){
            $this->options = $data['options'];
        }
        if(key_exists('2',$data)){
            $this->options = $data['2'];
        }
    }
}
