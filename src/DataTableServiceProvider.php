<?php

namespace Xsoft\DataTables;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;

class DataTableServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([__DIR__.'/../resources/public_share' => public_path(),], 'datatables-public');
        $this->publishes([__DIR__.'/../resources/views' => resource_path('/views/datatables'),], 'datatables-views');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'datatables');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->afterResolving('blade.compiler', function (BladeCompiler $bladeCompiler) {
            $bladeCompiler->directive('dataTable', function ($table) {
                return "<?php echo ($table)->draw(); ?>";
            });
        });
    }
}
