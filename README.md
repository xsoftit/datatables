# DataTables

Package for creating dataTables easily

## Install

`php artisan vendor:publish --tag=datatables-public`

## Basic usage

In controller use:

`Xsoft\DataTables\DataTable as DataTable`

create method for your table:

```
private function getTable()
{
 $table = new DataTable('tableOrModelName','routeToIndexAjax');
 return $table;
}
```

and use it

```
public function index()
{
    return response()->view('data.index',['dataTable' => $this->getTable()]);
}

public function indexAjax(Request $request)
{
    $output = $this->getTable()->output($request);
    return response()->json($output);
}
```

finally, in view use:

`@dataTable($dataTable)`

##

If you want to include datatables JS an CSS files, use _'datatables-js'_ and _'datatables-css'_
sections in your view. 

**WARNING!**
If you don't want to use sections mentioned above, you need set parameter
_noSection_ to _true_ of your table;

Example:

```
private function getTable()
{
 $table = new DataTable('tableOrModelName','routeToIndexAjax');
 $table->noSection = true;
 return $table;
}
```


## Advanced usage

**__construct()**

`$table = new DataTable('tableOrModelName','routeName','columns','options','params')

Construct accepts four parameters:
* model (model or table name)
* route (route name which return table output)
* columns (nullable, array of columns in format *['columnName','columnDatabaseField,'columnsOptions']*
)
* options (nullable, array in format *['option1' => '1', 'option2' => '2']*)
* params (route or ajax parameters passed to defined route in format *['paramName' => ['value','paramType']]*, paramType can by set on 'ajax' (default) or 'route')

All this table parameters can be set by setter functions **setModel(), setRoute(),
setColumns()**, **setOptions()** and **addParams()**

**join(...string|array)**

String example:

```
$table->join('products')
```

In this example, `products` table will be joined to our table via `OUR_TABLE.product_id` and `products.id`

Array example:

```
$table->join(['products','OUR_TABLE.prod_id','products.uid'])
```

In this example, `products` table will be joined to our table via `OUR_TABLE.prod_id` and `products.uid`

**searchBy(..string)**

Pass column names you want to search for

**orderBy(array)**

Pass array with two element: columns name and ordering direction `asc` or `desc`

**setQuery()**

Sets data to output

Example:

```
$query = User::where('active',1)->get();
$table->setQuery($query);
```

